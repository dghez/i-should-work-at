//TEST
jQuery(document).ready(function($) {

	/************************
	 		HELPERS 
	************************/
	function randNum(min,max){
	    return (Math.random()*(max-min)+min).toFixed(2);
	}


	/************************
	 		VARS 
	************************/
	var pressed 						= {status: false, time: 0},
		sectionInfo						= {active: 1, animable: true},
		body 							= $('body'),
		sectionTransitionContainer 		= $(".section-transition"),
		spacebar 						= $('#section--' + sectionInfo.active + ' .section-trigger-spacebar'),
		navTrigger 						= $('.nav-trigger'),
		circlesSection					= $('.section-transition-circle');

	var	tlHolding						= new TimelineMax({paused: true}),
		tlTransitionSection				= new TimelineMax({paused: true, onComplete: nextSection, onReverseComplete: function(){sectionInfo.animable = true;}}),
		tlPress 						= new TimelineMax(),
		tlFill 							= new TimelineMax();


	/************************
	 		TIMELINES 
	************************/
	circlesSection.each(function(i, el) {
		tlTransitionSection
			.fromTo(el, 1.2 , {scale: 0},{scale: randNum(0.6,1.3), ease:Expo.easeInOut}, i*0.075);
	});	



	function transitionSections(dir){
		if (dir == 'play') {
			sectionTransitionContainer.addClass('visible');
			tlTransitionSection.play();			
		}else if(dir == 'reverse'){
			tlTransitionSection.reverse();
		}
	}

	function pressAnimationGenerator(){
		tlPress = new TimelineMax({paused: true});
		tlFill 	= new TimelineMax({paused: true});
		tlPress
			.to($('#section--' + sectionInfo.active + ' .section-trigger-spacebar-text'), 0.05, {y:6});

		tlFill
			.to($('#section--' + sectionInfo.active + ' .section-trigger-spacebar-fill'), 1.6, {xPercent: 100, ease:Expo.easeInOut});
	}
	pressAnimationGenerator();


	/************************
	 		OPENING 
	************************/
	TweenMax.to($("#section--0"), 2.5, {height: '80vh', delay: 3, ease: Power4.easeOut});

	/************************
	 		BETWEEN SECTIONS STUFF 
	************************/
	function nextSection(){
		if($('#section--' + (sectionInfo.active + 1)).length > 0){
			$('#section--' + sectionInfo.active).removeClass('active').next().addClass('active').attr('data-status', 'revealed');
			$('#section--' + sectionInfo.active).find('.section-trigger').remove();
			sectionInfo.active += 1;	

			/** CHANGE COLOR BG **/
			if(body.hasClass('cp')){
				body.removeClass('cp').addClass('cs');
			}else{
				body.removeClass('cs').addClass('cp');
			}


			pressAnimationGenerator();
			$('body,html').scrollTop($('#section--' + sectionInfo.active).offset().top);
			sectionTransitionContainer.removeClass('visible');
			sectionInfo.animable = false;

			TweenMax.set('#section--' + sectionInfo.active + ' .section-trigger > span', {autoAlpha:0});
			TweenMax.staggerFromTo($('#section--' + sectionInfo.active).find('p'), 0.3, {y:40, autoAlpha: 0}, {y:0, autoAlpha:1, ease: Back.easeOut.config(1.2)}, 2, initScrollSectionTrigger);
		}else{
		}
	}

	$(document).on('keypress',function(event) {
		if(event.which === 32){
			event.preventDefault();
			if ((!pressed.status) && (sectionInfo.active <= 3) && ($('#section--' + sectionInfo.active + ' .section-trigger').hasClass('visible'))){
				console.log('triggerato visibile');
				pressed.status = true;
				if (sectionInfo.animable === true) {
					tlPress.play();
					tlFill.play();
					transitionSections('play');					
				}
			}
		}
	});
	$(document).on('keyup',function(event) {
		if(event.which === 32){
			event.preventDefault();
			pressed.status = false;
			tlPress.reverse();
			tlFill.reverse();
			transitionSections('reverse');			
		}
	});

	/************************
	 		SCROLLING
	************************/
	var controller = new ScrollMagic.Controller({loglevel: 3});

	function initScrollSectionTrigger(){
		var tweenTrigger = TweenMax.staggerFromTo('#section--' + sectionInfo.active + ' .section-trigger > span', 0.3, {y:40, autoAlpha:0}, {y:0, autoAlpha:1}, 0.05);
		var fadeInSectionTrigger = new ScrollMagic.Scene({
			triggerElement: '#section--' + sectionInfo.active + ' .question',
			triggerHook: 0.75
		})
		.setClassToggle('#section--' + sectionInfo.active + ' .section-trigger', 'visible')
		.setTween(tweenTrigger)
		.addIndicators()
		.addTo(controller);				
	}
	initScrollSectionTrigger();

	/************************
	 		MENU
	************************/

	var navItem 		= $('.nav-list a'),
		navBgContainer	= $('.nav-bgContainer'),
		navBg 			= $('.nav-bg'),
		navListBg 		= $('.nav-list-bg'),
		tlMenu 			= new TimelineMax({	paused: true, 
											// onComplete: function(){TweenMax.to($('.nav-list'), 0, {className:"+=finish"})},
											onReverseComplete: function(){ $('nav').removeClass('nav--active');}
						}),
		navItemHovered;

		tlMenu
			.fromTo(navListBg, 0.9, {xPercent: 100}, {xPercent: 0,  ease: Power4.easeInOut})
			.staggerFromTo((navItem).find('span'), 1.2, {x: 50, autoAlpha:0}, {x: 0, autoAlpha:1, ease: Power4.easeOut, clearProps: "opacity"}, 0.15, 0.65)
			.to($('.nav-list'), 0, {className:"+=nav-list--inside"});

		// tlMenu.timeScale(1.5);



	navTrigger.click(function(){
		if($('nav').hasClass('nav--active')){
			$('.nav-trigger').removeClass('nav-trigger--active');
			tlMenu.timeScale(2.5);
			tlMenu.reverse();
		}else{
			$('nav').addClass('nav--active');
			$('.nav-trigger').addClass('nav-trigger--active');
			tlMenu.timeScale(1);
			tlMenu.play();			
		}
	});
	navBgContainer.click(function(event) {
		tlMenu.timeScale(2.5);
		tlMenu.reverse();
	});

	navItem.hover(function() {
		navBgContainer.addClass('nav-bgContainer--active');
		navItemHovered = $(this).index();
		$(this).addClass('nav-list-item--active');
		navBg.eq(navItemHovered).addClass('nav-bg--active');
		navItem.not(this).addClass('nav-list-item--trasp');


	}, function() {
		navBgContainer.removeClass('nav-bgContainer--active');
		$(this).removeClass('nav-list-item--active');
		navBg.eq(navItemHovered).removeClass('nav-bg--active');
		navItem.removeClass('nav-list-item--trasp');
	});






}); // close doc.ready