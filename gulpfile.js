var gulp 							= require('gulp'),     
	sass 							= require('gulp-sass') ,
	notify 							= require("gulp-notify") ,
	sourcemaps 						= require('gulp-sourcemaps'),
	autoprefixer 					= require('gulp-autoprefixer'),
	concat 							= require('gulp-concat'),
	uglify 							= require('gulp-uglify'),
	mainBowerFiles					= require('main-bower-files'),
	filter 							= require('gulp-filter'),
	browserSync 					= require('browser-sync').create(),
	jshint 							= require('gulp-jshint'),
	stylish 						= require('jshint-stylish'),
	bower 							= require('gulp-bower');

var config = {
	sassPath: 	'./assets/**/*.scss',
	jsPath: 	'./assets/script/*.js',
	bowerDir: 	'./bower_components/*' 
}


gulp.task('sass', function () {
	return gulp
		.src(config.sassPath)
		.pipe(sourcemaps.init())
		.pipe(sass().on('error', sass.logError))
		.pipe(sourcemaps.write())
		.pipe(autoprefixer())
		.pipe(gulp.dest('./dist/'))
		.pipe(browserSync.stream());
});

gulp.task('lint', function(){
	return gulp
		.src(config.jsPath)
		.pipe(jshint())
		.pipe(jshint.reporter('jshint-stylish'));
});
gulp.task('js',['lint'], function() {
	var files = mainBowerFiles();
		files.push(config.jsPath);
		
	return gulp
		.src(files)
		.pipe(concat('all.min.js'))
		.pipe(uglify())
		.pipe(gulp.dest('./dist/script'))
		.pipe(browserSync.stream());
});


gulp.task('serve', ['sass'], function() {
		browserSync.init({
				server: "./"
		});
		gulp
		.watch([config.sassPath, config.jsPath], ['sass', 'js'])
		.on('change', function(event) {
			console.log('File ' + event.path + ' was ' + event.type + ', running tasks...');
		});

		gulp
		.watch("./*.html").on('change', browserSync.reload)
		.on('change', function(event) {
			console.log('File ' + event.path + ' was ' + event.type + ', running tasks...');
		});
});



gulp.task('watch', function() {
	return gulp
		.watch([config.sassPath, config.jsPath], ['sass', 'js'])
		.on('change', function(event) {
			console.log('File ' + event.path + ' was ' + event.type + ', running tasks...');
		});
});
